NOMone Shader Editor/Viewer
===========================

Open source MIT licensed shader editor and viewer. Heavily inspired by [glslsandbox (by mrdoob)](https://github.com/mrdoob/glsl-sandbox), and based on [Ace (Ajax.org Cloud9 Editor)](https://github.com/ajaxorg/ace).
Initially made to be used in a series of [shader tutorials](https://www.facebook.com/groups/graphics.shaders) by Omar El sayyed. Enables viewing and editing several shaders in a single page without collisions. Built with smartphone compatability in mind.

Embedding
---------

Can be easily embedded into any existing web page.

```html
   <div id="editor"></div>

   <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   <link rel="stylesheet" href="shaderEditor.css">

   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
   <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script src="ace/ace.js" type="text/javascript" charset="utf-8"></script>
   <script src="shaderEditor.js" type="text/javascript" charset="utf-8"></script>

   <script>
      var shaderEditor = new NOMoneShaderEditor(document.getElementById("editor"));
   </script>
```

With "editor" being the id of the DOM element, which should be converted into an editor. Note that the editor will try to consume all the space provided by this element, so it should be used to control the size of the editor. For example:

```css
   #editor {
      position: absolute;
      width: 480px;
      height: 320px;
   }
```

The editor can be manipulated using these functions:

```javascript
   shaderEditor.setFragmentShaderCode(fragmentShaderCode /*string*/); // Sets the code inside the editor.
   shaderEditor.compile();                                            // Compiles the code inside the editor.
   shaderEditor.setGLCanvasScale(scale /*float*/);                    // Changes the scale applied to the internal
                                                                      // canvas element. A larger number means
                                                                      // a smaller canvas (less quality but more
                                                                      // performance).
   shaderEditor.setUIVisibility(visible /*true-false*/);              // Removes all UI controls. Turns it into
                                                                      // a plain viewer with no options.
   shaderEditor.setEditorModeEnabled(isEditor /*true-false*/);        // Shows/hides the editor and all the controls 
                                                                      // associated with it. Turns it into a viewer
                                                                      // with a few options.
   shaderEditor.setCodeVisibility(visible /*true-false*/);            // Shows/Hides the code. However, "Show Code"
                                                                      // button can still be used to hide/restore it 
                                                                      // again.
   shaderEditor.setPlainEditorModeEnabled(enabled /*true-false*/);    // True for plain editor, false for cool editor.
```

### Demo

[Project page.](http://nomone.com/shaderEditor)

